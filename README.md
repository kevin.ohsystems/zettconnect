# Exploring Ideas For a Collaborative Knowledge Management System

## Problem Statement

**A:** In developing ideas or strategies in organizations, thoughts are written and links are posted in team chats, email threads etc. The context and thinking behind the initial idea are easily forgotten and the message thus lost.

An element in research and development that is often neglected is to describe the connections: Why does A link to B, hence the context is lost.

This _connection_ is often part of the message (in a chat for example where a link to an article is posted with a remark such as "this article explains why we might run into problems with our strategy because of unconscious bias"). Such comments mostly get lost in the communication streams, yet they are valuable work and material in that they **show and store the thinking** someone did.

**B:** Thought processes (ideas) are much easier to understand and to collaborate on if they are visualized. At same time we want to stick to **the principle of storing everything in plain text**, so what we write can go into version control (git) and can be fed to machines if we want to do things like [natural language processing](https://en.wikipedia.org/wiki/Natural_language_processing), search and other computer based processing of our writing (our thoughts).

## Possible Solution

A combination of systems and languages like [Zettelkasten](https://en.wikipedia.org/wiki/Zettelkasten), [Business Process Model and Notation](https://en.wikipedia.org/wiki/Business_Process_Model_and_Notation), a [markup language](https://en.wikipedia.org/wiki/Business_Process_Model_and_Notation) like [markdown](https://en.wikipedia.org/wiki/Markdown), [Git version control](https://en.wikipedia.org/wiki/Git), a unified process and workplace integration.
Integrate with the digital workplace through a central chat application like Slack, using chatbots or slash commands.

## Workflow Diagram

![Workflow Diagram](/diagrams/zettconnect-workflow-v2.png "Zettconnect Workflow Diagram")

## Further Ideas

With the stored zettel we could do:
* automatic gouping by topic with NLP
* automatic grouping by author
* build views of the zettel and their connections


## Known Issues

Please see the projects [issues](https://gitlab.com/kevin.ohsystems/zettconnect/-/issues) view.
