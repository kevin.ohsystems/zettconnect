# The Basis for Good Collaborative Knowledge Management

This is an outline of what it takes for **successful knowledge management in teams / organizations**.

## How-To

1. Create an organizational chart of your team or organization with responsibilities for knowledge topics assigned to roles. Assign people to roles.

2. Create a policy for knowledge management in your team or organization

3. Keep the policy simple and workable. Each shared item should have no more than 3 conditions for sharing, for example

- a) What led me to this idea
- b) In what context do I see it
- c) Where might it lead


If you add more, sharing an idea is prone to be too much work.

4. Authorship: Define how editing of content should work. Ideally every content has an owner responsible to keep the content up to date, but everyone can and should edit if they see something needs updating.

5. Add comments to changes

6. Use version control, make the history accessible

7. Add analytics. If content is never accessed, content owners need to know it and act by changing or deleting the content.

8. Add links and space to explain the connections, note down why something is linked

9. Edit in one place. Avoid a structure where you need to take notes of a necessary change to apply it later in another system.

10. Own your content. Make sure you can export your knowledge and re-use it in another system.

11. Make it machine-readable, so it can be indexed by search engines and accessed for Natural Language Processing and machine learning.

12. Think about how knowledge can be visualized, for example as a mind-map

13. Decide on a common tool, avoid mixing several knowledge management applications.
