# Our Knowledge Management Policy

This is how we manage knowledge in our team

### Roles and Responsibilities

An Organizational chart with the knowledge-manager role created and assigned to each topic (department / circle / area of expertise / customer segment...). This chart is a living document. The role and topic assignment is to be updated in respective team meetings. [link to chart](./orga_chart.md)

### Structure of Shared Items

With every link or idea we share, we write out the following questions and answers:

- a) What led me to this idea
- b) In what context do I see it (relevance to our work)
- c) Where might it lead

### Responsibilities

1. Each knowledge item must have an owner, who needs to track the relevance of the item over time (update or delete).
2. It is the responsibility of everyone, to fix (update) an item when we see a problem.
3. Everyone creating an item is responsible to ask the relevant role to take ownership, until the role holder accepts the item, the creator remains the owner.
