---
Author: "Kevin Mueller"
Title: "Cybernetics"
SourceRef: "202104191055_Selvini" 
ContextRef: "202104151410_Changemanagement"
TargetRef: ""
BibResource: "/bibliography.bib"
---


# Cybernetics

## What brought me here

Contemplating the request our team received to define the problem question for the "change management request", I thought about what stops organizations from developing.

## What I'm citing

I remembered Maria Selvini's work [^1] on how **communication- and relationship patterns of top management influence the organization** as a whole. 

## How does this relate to my project

In the literature around organizational development the psychology of the participants in a system keeps coming up. Selvini offers an easy to understand real-life example of how the communication and behaviour of management influences the development of an organization. I think we should consider this for our organization too, working with a psychologist to analyze the workings of an example department, maybe our own even.

4. Where could this lead? **TargetRef** if this Zettel was sourced from TargetRef

[^1]: [@enwiki:1017010838]
