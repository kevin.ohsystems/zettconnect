---
Author: "Kevin Mueller"
Title: "Cybernetics"
SourceRef: "202104191055_Selvini" 
TargetRef: ""
BibResource: "/bibliography.bib"
---


# Subject

## Questions regarding this Zettel

1. **What brought me here?** Link to other Zettel and explanation of _trail of thought_: **SourceRef**
2. **What am I citing?** citation using footnote and bibtex [^1], author description and abstract of publication
3. **How does this relate to my project?** Note down your thoughts on why this Zettel is relevant for the context (your work / project / goal / vision...) **ContextRef**
4. Where could this lead? **TargetRef** if this Zettel can or should be followed up by a related Zettel, rather than making this one unnecessarily big (split your thoughts in zettel-sized chunks to allow for a modularisation of the collaborative development process etc)

[^1]: [@enwiki:1017010838]
